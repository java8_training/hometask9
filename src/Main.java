import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Main {

    static LocalDate getDateFromUser() {
        Scanner sc = new Scanner(System.in);
        int day, month, year;
        System.out.print("Enter the day (1-31): ");
        day = sc.nextInt();
        System.out.print("Enter the month (1-12): ");
        month = sc.nextInt();
        System.out.print("Enter the year (4 digits): ");
        year = sc.nextInt();
        return LocalDate.of(year, month, day);
    }

    public static void main(String[] args) {

        //Predicate to check if the given date is yesterday date.
        Predicate<LocalDate> checkDate = (userDate) -> userDate.equals(LocalDate.now().minusDays(1));
        System.out.println("Enter the date to check (dd/mm/yyyy): ");
        LocalDate userProvidedDate = getDateFromUser();
        if (checkDate.test(userProvidedDate))
            System.out.println("Give date is yesterday's date");
        else System.out.println("Give date is not yesterday's date");

        //Supplier to get Date for next Thursday.
        Supplier<LocalDate> getNextThursday = () -> LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.THURSDAY));
        System.out.println("Next Thursday : " + getNextThursday.get());

        //Supplier to get CurrentTime in EST timezone
        Supplier<LocalDateTime> getESTTime = () -> LocalDateTime.now(ZoneId.of("America/New_York"));
        System.out.println("Current time in EST : " + getESTTime.get());

        //Function to calculate the age of a person given date of birth.
        Function<LocalDate, Period> calculateAgeOfPerson = (dateOfBirth) -> {
            LocalDate currentDate = LocalDate.now();
            return Period.between(dateOfBirth, currentDate);
        };
        System.out.println("Enter the dob (dd/mm/yyyy): ");
        LocalDate dob = getDateFromUser();
        Period age = calculateAgeOfPerson.apply(dob);
        System.out.printf("Age is : %d years %d months %d days",
                age.getYears(), age.getMonths(), age.getDays());
    }
}
